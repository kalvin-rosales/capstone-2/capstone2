const express = require('express')
const router = express.Router()

const User = require('./../models/User')
const Product = require('./../models/Product')
const Order = require('./../models/Order')

const { create,
	getAOrder,
	getAllOrder,
	deleteOrder,
	order

}= require('./../controllers/orderControllers')

const {verifyAdmin, decode,verify} = require('./../auth')

router.post('/create',verify, async (req, res) => {
	// console.log(req.body)
	const user = decode(req.headers.authorization).isAdmin
	if(user == false){
	try{
		create(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
	} else {
		res.send(`Only with user access can create an order`)
	}
})

router.get('/:orderId', verify, async (req, res) => {
	try{
		await getAOrder(req.params.orderId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.get('/', verifyAdmin, async (req, res) => {

	try{
		await getAllOrder().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.delete('/:orderId/delete-order', verifyAdmin, async (req, res) => {
	const deleteid = req.params.orderId
	try{
		await deleteOrder(deleteid).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


router.post('/order',verify, async (req,res) => {
	const user = decode(req.headers.authorization).isAdmin
	const data = {
		userId: decode(req.headers.authorization).id,
		productId: req.body.productId,
		orderId: req.body.orderId,
	}
	console.log(data)
	if(user == false){
		try{
			await order(data).then(result => res.send(result))
		} catch(err){
			res.status(500).json(err)
		}
	} else {
		res.send(`Only with user access can order`)
	}
})






module.exports = router