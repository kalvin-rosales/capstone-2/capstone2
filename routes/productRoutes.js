const express = require('express')
const router = express.Router()


const {create,
	activeProducts,
	getAProduct,
	updateProduct,
	archive,
	unArchive,
	deleteProduct

} = require('./../controllers/productControllers')

const {verifyAdmin, decode, verify} = require('./../auth')


router.post('/createproduct', verifyAdmin, async (req, res) => {
	// console.log(req.body)
	const admin = decode(req.headers.authorization).isAdmin

	if(admin){
	try{
		create(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
	} else {
		return false
	}
})


router.get('/activeproducts', verify, async (req, res) => {
	try{
		await activeProducts().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.get('/:productId', verify, async (req, res) => {
	try{
		await getAProduct(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.put('/:productId/update', verifyAdmin, async (req, res) => {
	try{
		await updateProduct(req.params.productId, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


router.put('/:productId/archive', verifyAdmin, async (req, res) => {
	try{
		await archive(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


router.put('/:productId/unarchive', verifyAdmin, async (req, res) => {
	try{
		await unArchive(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


router.delete('/:productId/delete-product', verifyAdmin, async (req, res) => {
	const deleteid = req.params.productId
	try{
		await deleteProduct(deleteid).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



module.exports = router