const express = require('express');
const router = express.Router();

const {
	register,
	getAllUsers,
	profile,
	login,
	adminStatus,
	deleteUser,
	order
	} = require('./../controllers/userControllers');

const {verify, decode, verifyAdmin} = require('./../auth');


router.post('/register', async (req, res) => {
	 //console.log(req.body)	

	try{
		await register(req.body).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})


router.get('/', async (req, res) => {

	try{
		await getAllUsers().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



router.get('/profile', verify, async (req, res) => {
	// console.log(req.headers.authorization)
	const userId = decode(req.headers.authorization).id
	// console.log(userId)

	try{
		profile(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.post('/login', (req, res) => {
	// console.log(req.body)
	try{
		login(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


router.put('/:userId/setAsAdmin', verifyAdmin, async (req, res) => {
	const id = req.params.userId
	try{
		await adminStatus(id, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})

router.delete('/:userId/delete-user', verifyAdmin, async (req, res) => {
	const deleteid = req.params.userId
	try{
		await deleteUser(deleteid).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



module.exports = router;