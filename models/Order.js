const mongoose = require('mongoose');


const orderSchema = new mongoose.Schema({

	totalAmount: {
	    type: Number,
	    required: [true, `Total amount is required`],
	},

    purchasedOn: {
        type: Date,
        default: new Date()
    },
    incomingOrders: [
        {
            userId: {
                type: String,
                required: [true, `userId is required`]
            },
            
            productId: {
            	type: String,
            	required: [true, `Product ID is required`]
            },
            orderedOn: {
                type: Date,
                default: new Date()
            }
        }
    ]

}, {timestamps: true})

module.exports = mongoose.model("Order", orderSchema);