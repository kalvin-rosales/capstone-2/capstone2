const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors')


const PORT = process.env.PORT || 3011;
const app = express();

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes')

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())


mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true})


const db = mongoose.connection;
db.on("error", console.error.bind(console, 'connection error:'))
db.once("open", () => console.log(`Connected to Database`))




app.use(`/api/user`, userRoutes);
app.use(`/api/product`, productRoutes);
app.use(`/api/order`, orderRoutes);



app.listen(PORT, () => console.log(`Server connected to the ${PORT}`))