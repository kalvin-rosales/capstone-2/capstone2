const CryptoJS = require("crypto-js");
const User = require('./../models/User')

const {createToken} = require('./../auth');

module.exports.register = async (reqBody) => {
	// console.log(reqBody)
	const {firstName, lastName, email, password} = reqBody

	const newUser = new User({
		firstName: firstName,
		lastName: lastName, 
		email: email,
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
		//password: bcrypt.hashSync(password, 10)
	})

	return await  newUser.save().then(result => {
		if(result){
			return result
		} else {
			if(result == null){
				return false
			}
		}
	})
}



module.exports.getAllUsers = async () => {

	return await User.find().then(result => result)
}






module.exports.profile = async (id) => {

	return await User.findById(id).then((result, err) => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message:`user does not exist`}
			} else {
				return err
			}
		}
	})
}


module.exports.login = async (reqBody) =>{

	return await User.findOne({email: reqBody.email}).then((result, err)=> {
		if(result == null){
			return {message: `User does not exist.`}

		} else {

			if(result !== null){
				//check if password is correct
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

				console.log(reqBody.password==decryptedPw)

				if(reqBody.password == decryptedPw){
						//create a token for the user

						return {token:createToken(result)}			
				} else {
						return {auth: `Auth Failed`}
				}

			} else {
				return err
			}
		}
	})
}

module.exports.adminStatus = async (id,reqBody) => {
	

	return await User.findByIdAndUpdate(id, {$set: {isAdmin: true}}, {new:true}).then((result, err) => result ? true : err)
}

module.exports.deleteUser = async (deleteId) => {

	return await User.findByIdAndDelete(deleteId).then((result, err) => result ? `You have deleted the user` : err)
}


