const Order = require('./../models/Order');

const User = require('./../models/User');

const Product = require(`./../models/Product`)

const {createToken} = require('./../auth');

module.exports.create = async (reqBody) => {
	const {totalAmount,userId,productId} = reqBody

	let newOrder = new Order({
		totalAmount: totalAmount,
		userId:userId,
		productId:productId
	
	})
	return await newOrder.save().then((result, err) => result ? result : err)
}

module.exports.getAOrder = async (id) => {

	return await Order.findById(id).then((result, err) => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message: `Order not found`}
			}else{
				return err
			}
		}
	})
}




module.exports.getAllOrder = async () => {
	return await Order.find().then(result => result)
}


module.exports.deleteOrder = async (deleteId) => {

	return await Order.findByIdAndDelete(deleteId).then((result, err) => result ? `You have deleted the order` : err)
}



module.exports.order = async (data) =>{
	const{userId, productId, courseId} = data

	const updatedUser = await User.findById(userId).then(result => {
		result.orders.push({orderId: orderId})

		return result.save().then(user => user ? true : false)
	})


	const updatedProduct = await Course.findById(productId).then(result => {
		result.orders.push({orderId:orderId})
		return result.save().then(user => user ? true : false)
	})

	const updatedOrderUser = await User.findById(orderId).then(result => {
		result.incomingOrders.push({userId: userId})

		return result.save().then(order => order ? true : false)
	})

	const updatedOrderProduct = await Course.findById(productId).then(result => {
		result.incomingOrders.push({userId:userId})
		return result.save().then(order => order ? true : false)
	})

	if(updatedUser && updatedProduct && updatedOrderUser && updatedOrderProduct){
		return true
	} else {
		false
	}

}